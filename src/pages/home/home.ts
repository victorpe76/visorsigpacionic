import { Component, NgZone } from '@angular/core';

import { NavController } from 'ionic-angular';
declare var CordovaPluginVisorsigpac;
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  resultadovisor:string='Seleccione una parcela en el visor';
  
  constructor(public zone: NgZone,public navCtrl: NavController) {
    
  }
gotoVisorSigpac(){
  console.log("Voy a intentar lalmar al plugin")
    this.resultadovisor='Iniciando el visor ...';
   CordovaPluginVisorsigpac.launchvisor((response:string) => {
   this.resultadovisor='Hemos obtenido respuesta';
     console.log("La respuesta obtenida del servidor es " + response);
     this.zone.run(() => this.resultadovisor = response)
     
     
   });
  }
  
  
}
