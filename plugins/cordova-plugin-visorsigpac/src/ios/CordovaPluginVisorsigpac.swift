import VisorSigpacFrm

@objc(CordovaPluginVisorsigpac) class CordovaPluginVisorsigpac : CDVPlugin,VisorDelegate {
    var currentcommand:CDVInvokedUrlCommand!;
    var vc:VisorNavigationController!
    func onSelectParcela(_ datos: String){
        var pluginResult = CDVPluginResult(
            status: CDVCommandStatus_OK,
            messageAs: datos
        )
        self.commandDelegate!.send(
            pluginResult,
            callbackId: currentcommand.callbackId
        )
        self.vc.dismiss(animated: false, completion: nil)

    }
    func launchvisor(_ command: CDVInvokedUrlCommand) {
        print("Intentan lanzar el plugin")
        self.currentcommand=command;




        let s = UIStoryboard (
            name: "visor", bundle: Bundle(for: VisorNavigationController.self)
        )


        self.vc = s.instantiateInitialViewController() as! VisorNavigationController
        self.vc.visordelegate=self
        self.viewController?.present(self.vc, animated: true, completion: nil)


    }

    override func pluginInitialize() {
        print("Se ha inicializado el plugin")
    }
}
