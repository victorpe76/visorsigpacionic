/**
 */
package com.aidiapp;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;
import org.apache.cordova.PluginResult.Status;
import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;
import aidiapp.com.visorsigpac.VisorActivity;

import android.content.Intent;
import android.util.Log;

import java.util.Date;

public class CordovaPluginVisorsigpac extends CordovaPlugin {
  private static final String TAG = "MyCordovaPlugin";
  private CallbackContext callbackContext;
  public void initialize(CordovaInterface cordova, CordovaWebView webView) {
    super.initialize(cordova, webView);

    Log.d(TAG, "Initializing MyCordovaPlugin");
  }

  public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) throws JSONException {
    this.callbackContext=callbackContext;
    if(action.equals("launchvisor")) {
      Intent intent=new Intent(this.cordova.getActivity(),VisorActivity.class);
      this.cordova.startActivityForResult(this,intent,1);

    } 
    return true;
  }

  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    if(requestCode==1 && resultCode==10){
      String response=data.getStringExtra(VisorActivity.RESPONSEVISOR);
      Log.d("MAIN",response);
      this.callbackContext.success(response);

    }

  }

}
