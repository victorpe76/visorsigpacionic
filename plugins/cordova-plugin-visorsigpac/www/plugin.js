
var exec = require('cordova/exec');

var PLUGIN_NAME = 'CordovaPluginVisorsigpac';

var CordovaPluginVisorsigpac = {
  launchvisor: function( cb) {
	  console.log("Intentamos invocar el plugin "+PLUGIN_NAME);
    exec(cb, null, PLUGIN_NAME, 'launchvisor', []);
  }
};

module.exports = CordovaPluginVisorsigpac;
